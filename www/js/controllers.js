angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('VeillesCtrl', function($scope, VeillesFactory) {
  $scope.veilles = [];
   VeillesFactory.getAll().then(function(r){
    $scope.veilles = r.data.veilles;
    console.log($scope.veilles);
  });
})

.controller('VeilleDetailCtrl', function($scope, $stateParams, VeillesFactory) {
  VeillesFactory.get($stateParams.veilleId).then(function(r){
    $scope.veille = r.data.veille;
    console.log($scope.veille);
  });
  console.log($stateParams.veilleId);
})

.controller('UsersCtrl', function($scope, UsersFactory) {
  $scope.users = [];
   UsersFactory.getAll().then(function(r){
    $scope.users = r.data.users;
    console.log($scope.users);
  });
})
.controller('UserDetailCtrl', function($scope, $stateParams, UsersFactory) {
  console.log($stateParams.userId);
   UsersFactory.get($stateParams.userId).then(function(r){
    $scope.user = r.data.user;
    console.log(r);
  });
  UsersFactory.getveilles($stateParams.userId).then(function(t){
  $scope.veilles = t.data.veilles;
 });
})

.controller('LogCtrl', function($scope, UsersFactory) {
  $scope.data = {};
  $scope.login = function(){
    console.log($scope.data.password, $scope.data.email);
    UsersFactory.auth($scope.data.email,$scope.data.password).then(function(t){
      if(t.data.status=="ok"){
        sessionStorage.email = $scope.data.email;
        sessionStorage.password = $scope.data.password;
        sessionStorage.id_user = t.data.user[0].id;
        sessionStorage.firstname = t.data.user[0].firstname;
        console.log("ok");
        return true;
      }
      else{
        console.log("no");
        return false;
      }
    });
  }
})

.controller('PublishCtrl', function($scope, $location, UsersFactory) {
  if(typeof sessionStorage.email !== 'undefined'  &&
     typeof sessionStorage.password !== 'undefined'){
       console.log('auth');
     }
  else{
    console.log('Pas auth');
    $location.path('/tab/log');
  }

  $scope.data = {};
  $scope.pub = function(){
    console.log($scope.data.choice, $scope.data.titl, $scope.data.date, $scope.data.src, $scope.data.lin, $scope.data.desc);
    UsersFactory.save({api: "veille", action:"save", email: sessionStorage.email, password: sessionStorage.password,
    veille:{"id_user": sessionStorage.id_user,
    "firstname": "Ingrid",
    "type":$scope.data.choice,
    "title":$scope.data.titl,
    "date":$scope.data.date,
    "source":$scope.data.src,
    "link":$scope.data.lin,
    "description":$scope.data.desc}}).then(function (res){
      console.log(res);
      if(res.data.status == "ok") {
        console.log('done');
      }
      else {
        console.log('nope');
      }
      $scope.response = res.data;
    });
  }

})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
